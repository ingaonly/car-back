import { Injectable, Logger } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UsersStorageService } from '@services/storage/users/users-storage.service';
import { JwtService } from '@nestjs/jwt';
import { AuthUserDto } from '@users/dto/auth-user.dto';
import { UserDto } from '@users/dto/full-user.dto';

@Injectable()
export class UsersService {

  private readonly logger = new Logger(UsersService.name);

  constructor(private userStorageService: UsersStorageService,
              private jwtService: JwtService) {
  }

  /**
   * Создание пользователя
   * @param createUserDto
   */
  async create(createUserDto: CreateUserDto): Promise<UserDto> {
    return this.userStorageService.create(createUserDto)
      .then((user) => {
        return this.userStorageService.documentToDTO(user);
      });
  }

  // async findAll(): Promise<User[]> {
  //   return this.userStorageService.findAll();
  // }

  /**
   * Получение пользователя по id
   * @param id
   */
  async findOneByID(id: string): Promise<UserDto> {
    return this.userStorageService.findOneByID(id)
      .then((user) => {
        return this.userStorageService.documentToDTO(user);
      });
  }

  /**
   * Обновление пользователя
   * @param id
   * @param updateUserDto
   */
  async update(id: string, updateUserDto: UpdateUserDto): Promise<UserDto> {
    return this.userStorageService.update(id, updateUserDto)
      .then((user) => {
        return this.userStorageService.documentToDTO(user);
      });
  }

  async checkAuthUser(email: string, password: string): Promise<UserDto> {
    return this.userStorageService.findOneByEmailAndPassword(email, password)
      .then((user) => {
        return this.userStorageService.documentToDTO(user);
      });
  }

  async checkRegUser(email: string): Promise<UserDto> {
    return this.userStorageService.findOneByEmail(email)
      .then((user) => {
        return this.userStorageService.documentToDTO(user);
      });
  }

  async login(data: AuthUserDto) {
    const userFromDB = await this.userStorageService.findOneByEmailAndPassword(data.email, data.password);
    if (!userFromDB) {
      return {};
    }
    this.logger.debug(userFromDB);
    const payload = { id: userFromDB._id };
    return {
      id: userFromDB._id,
      access_token: this.jwtService.sign(payload),
    };
  }
}
