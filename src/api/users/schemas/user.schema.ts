import { UserInterface } from '@interfaces/user.interface';
import { HydratedDocument } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type UserDocument = HydratedDocument<User>;

@Schema()
export class User implements UserInterface {
  @Prop() name: string;

  @Prop() email: string;

  @Prop({ select: false })
  password: string;

  @Prop() phone: string;

  _id?: string;
}

export const UserSchema = SchemaFactory.createForClass(User);