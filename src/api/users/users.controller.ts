import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  UseGuards,
  Request,
  HttpException,
  HttpStatus, Logger,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { AuthUserDto } from '@users/dto/auth-user.dto';
import { JwtAuthGuardService } from '@services/authentitication/jwt-auth.guard/jwt-auth.guard.service';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { FullUserDto, UserDto } from '@users/dto/full-user.dto';

@Controller({
  path: 'users',
  version: '1',
})
export class UsersController {
  private readonly logger = new Logger(UsersController.name);

  constructor(private readonly usersService: UsersService) {
  }

  @ApiOperation({ summary: 'Создание пользователя' })
  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    this.logger.log(createUserDto);
    return this.usersService.checkRegUser(createUserDto.email)
      .then((queryRes) => {
        this.logger.log('data reg', queryRes);
        if (!queryRes) {
          return this.usersService
            .create(createUserDto)
            .catch((err) => {
              this.logger.error('err', err);
              throw new HttpException(
                {
                  status: HttpStatus.INTERNAL_SERVER_ERROR,
                  errorText: 'Что-то пошло не так...',
                },
                HttpStatus.INTERNAL_SERVER_ERROR,
              );
            });
        } else {
          this.logger.log('err - user is exists with email' + createUserDto.email);
          throw new HttpException(
            {
              status: HttpStatus.CONFLICT,
              errorText: 'Пользователь с таким email уже существует',
            },
            HttpStatus.CONFLICT,
          );
        }
      });
  }

  @ApiOperation({ summary: 'Обновление пользователя' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardService)
  @Put(':me')
  update(@Body() updateUserDto: UpdateUserDto, @Request() req) {
    return this.usersService.update(req.user.id, updateUserDto);
  }

  @ApiOperation({ summary: 'Авторизация' })
  @UseGuards(AuthGuard('local'))
  @Post(':login')
  authUser(@Body() data: AuthUserDto, @Param('login') login): any {
    return this.usersService.login(data);
  }

  @ApiOperation({ summary: 'Получение пользователем информации о себе' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardService)
  @Get(':me')
  @ApiResponse({
    status: 200,
    type: FullUserDto,
  })
  async findOne(@Request() req): Promise<UserDto> {
    return this.usersService.findOneByID(req.user.id)
  }

  // @Get()
  // findAll() {
  //   return this.usersService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.usersService.findOne(id);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.usersService.remove(id);
  // }
}
