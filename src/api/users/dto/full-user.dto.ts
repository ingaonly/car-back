import { ApiProperty } from '@nestjs/swagger';
import { UserInterface } from '@interfaces/user.interface';

export class UserDto {
  @ApiProperty({ example: 'Иван', description: 'Имя пользователя системы', type: 'string' })
  readonly name: string;

  @ApiProperty({ example: 'mail@example.com', description: 'Email пользователя системы', type: 'string' })
  readonly email: string;

  // @ApiProperty({ example: 'password', description: 'Пароль пользователя системы', type: 'string' })
  // readonly password: string;

  @ApiProperty({ example: '+77777777777', description: 'Телефон пользователя системы', type: 'string' })
  readonly phone: string;

  @ApiProperty({ example: '63cd7c5edea0d8da68249a68', description: 'ID пользователя', type: 'string' })
  _id: string;

}

export class FullUserDto extends UserDto implements UserInterface {
  @ApiProperty({ example: 'password', description: 'Пароль пользователя системы', type: 'string' })
  readonly password: string;
}