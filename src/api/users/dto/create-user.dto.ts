import { UserInterface } from '@interfaces/user.interface';
import { FullUserDto } from '@users/dto/full-user.dto';

export class CreateUserDto extends FullUserDto implements UserInterface{}
