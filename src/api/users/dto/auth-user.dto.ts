import { PickType } from '@nestjs/mapped-types';
import { FullUserDto } from '@users/dto/full-user.dto';

export class AuthUserDto extends PickType(FullUserDto, ['email', 'password']) {
}