import { PartialType } from '@nestjs/mapped-types';
import { FullUserDto } from '@users/dto/full-user.dto';

export class UpdateUserDto extends PartialType(FullUserDto) {
}
