import { Module } from '@nestjs/common';
import { UsersModule } from '@users/users.module';
import { CarsModule } from './cars/cars.module';
import { CarBrandsModule } from './car-brands/car-brands.module';
import { CarModelsModule } from './car-models/car-models.module';
import { OrdersModule } from './orders/orders.module';

@Module({
  imports: [
    UsersModule,
    CarsModule,
    CarBrandsModule,
    CarModelsModule,
    OrdersModule,
  ],
  controllers: [],
})
export class ApiModule {
}
