import { Module } from '@nestjs/common';
import { CarBrandsService } from './car-brands.service';
import { CarBrandsController } from './car-brands.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UsersModule } from '@users/users.module';
import { CarBrandsStorageService } from '@services/storage/car-brands/car-brands-storage.service';
import { CarBrand, CarBrandSchema } from './schemas/car-brand.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: CarBrand.name, schema: CarBrandSchema }]),
    PassportModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        secret: config.get('JWT_SECRET'),
      }),
      inject: [ConfigService],
    }),
    UsersModule,
  ],
  controllers: [CarBrandsController],
  providers: [CarBrandsService, CarBrandsStorageService],
  exports: [CarBrandsService]
})
export class CarBrandsModule {
}
