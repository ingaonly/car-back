import { Injectable } from '@nestjs/common';
import { CreateCarBrandDto } from './dto/create-car-brand.dto';
import { CarBrandsStorageService } from '@services/storage/car-brands/car-brands-storage.service';
import { UpdateCarBrandDto } from '@api/car-brands/dto/update-car-brand.dto';
import { CarBrandDto } from '@api/car-brands/dto/car-brand.dto';

@Injectable()
export class CarBrandsService {
  constructor(private readonly carBrandStorageService: CarBrandsStorageService) {
  }

  async create(createCarBrand: CreateCarBrandDto): Promise<CarBrandDto> {
    return this.carBrandStorageService.create(createCarBrand)
      .then((carBrand) => {
        return this.carBrandStorageService.documentToDTO(carBrand);
      });
  }

  async findAll(): Promise<CarBrandDto[]> {
    return this.carBrandStorageService.findAll()
      .then((carBrands) => {
        return carBrands.map((carBrand) => {
          return this.carBrandStorageService.documentToDTO(carBrand);
        });
      });
  }

  async findByIDs(ids: string[]): Promise<CarBrandDto[]> {
    return this.carBrandStorageService.findByIDs(ids)
      .then((carBrands) => {
        return carBrands.map((carBrand) => {
          return this.carBrandStorageService.documentToDTO(carBrand);
        });
      });
  }

  async findOne(id: string): Promise<CarBrandDto> {
    return this.carBrandStorageService.findOne(id)
      .then((carBrand) => {
        return this.carBrandStorageService.documentToDTO(carBrand);
      });
  }

  async update(id: string, body: UpdateCarBrandDto): Promise<CarBrandDto> {
    return this.carBrandStorageService.update(id, body)
      .then((carBrand) => {
        return this.carBrandStorageService.documentToDTO(carBrand);
      });
  }

}
