import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { CarBrandInterface } from '@interfaces/car-brand.interface';

export type CarBrandDocument = HydratedDocument<CarBrand>

@Schema()
export class CarBrand implements CarBrandInterface {
  @Prop({unique: true})
  name: string;
}

export const CarBrandSchema = SchemaFactory.createForClass(CarBrand);