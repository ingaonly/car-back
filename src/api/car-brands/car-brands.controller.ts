import { Controller, Get, Post, Body, Param, UseGuards, Logger, HttpException, HttpStatus, Put } from '@nestjs/common';
import { CarBrandsService } from './car-brands.service';
import { CreateCarBrandDto } from './dto/create-car-brand.dto';
import { JwtAuthGuardService } from '@services/authentitication/jwt-auth.guard/jwt-auth.guard.service';
import { UpdateCarBrandDto } from '@api/car-brands/dto/update-car-brand.dto';
import { CarBrand } from '@api/car-brands/schemas/car-brand.schema';
import { ApiBearerAuth, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { CarBrandDto } from '@api/car-brands/dto/car-brand.dto';

@Controller({
  path: 'car-brands',
  version: '1',
})
export class CarBrandsController {
  private readonly logger = new Logger(CarBrandsController.name);

  constructor(private readonly carBrandsService: CarBrandsService) {
  }


  @ApiResponse({
    status: 200,
    type: CarBrandDto,
  })
  @ApiOperation({ summary: "Создание бренда" })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardService)
  @Post()
  create(@Body() createCarBrandDto: CreateCarBrandDto) {
    return this.carBrandsService
      .create(createCarBrandDto)
      .catch((err) => {
        if (err.name === 'MongoServerError' && err.code === 11000) {
          this.logger.log('Car brand already exists: ' + createCarBrandDto.name);
          throw new HttpException(
            {
              status: HttpStatus.UNPROCESSABLE_ENTITY,
              errorText: 'Бренд уже существует',
            },
            HttpStatus.UNPROCESSABLE_ENTITY,
          );
        }
        this.logger.error(err);
        throw new HttpException(
          {
            status: HttpStatus.INTERNAL_SERVER_ERROR,
            errorText: 'Что-то пошло не так...',
          },
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      });
  }

  @ApiResponse({
    status: 200,
    type: CarBrandDto,
    isArray: true,
  })
  @ApiOperation({ summary: "Получить все бренда" })
  @Get() //car-brands
  async findAll(): Promise<CarBrandDto[]> {
    return this.carBrandsService.findAll();
  }

  @ApiResponse({
    status: 200,
    type: CarBrandDto,
  })
  @ApiOperation({ summary: "Получить бренд по id" })
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<CarBrandDto> {
    return this.carBrandsService.findOne(id);
  }

  @ApiResponse({
    status: 200,
  })
  @ApiOperation({ summary: "Обновление бренда по id" })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardService)
  @Put(':id')
  async update(@Param('id') id, @Body() data:UpdateCarBrandDto): Promise<CarBrand>{
    this.logger.log(data);
    return this.carBrandsService.update(id, data);
  }
}
