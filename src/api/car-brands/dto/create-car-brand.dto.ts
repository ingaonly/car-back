import { CarBrandInterface } from '@interfaces/car-brand.interface';
import { ApiProperty } from '@nestjs/swagger';

export class CreateCarBrandDto implements CarBrandInterface {
  @ApiProperty({ example: "BMW", description: 'The car brand name' })
  name: string;
}

