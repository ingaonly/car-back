import { CarBrandInterface } from '@interfaces/car-brand.interface';
import { ApiProperty } from '@nestjs/swagger';

export class CarBrandDto implements CarBrandInterface {
  @ApiProperty({ example: "BMW", description: 'Бренд автомобиля', type: 'string' })
  name: string;

  @ApiProperty({ example: "63cd7c5edea0d8da68249a68", description: 'ID бренда', type: 'string' })
  _id: string;
}

