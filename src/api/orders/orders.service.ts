import { Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { OrdersStorageService } from '@services/storage/orders/orders-storage.service';
import { FullOrderDto } from '@api/orders/dto/full-order.dto';

@Injectable()
export class OrdersService {

  constructor(private readonly ordersStorageService: OrdersStorageService) {

  }

  async create(createOrderDto: CreateOrderDto) {
    return this.ordersStorageService.create(createOrderDto);
  }

  async findAll(): Promise<FullOrderDto[]> {
    return this.ordersStorageService.findAll()
      .then((orders) => {
        return orders.map((order) => {
          return this.ordersStorageService.documentToDTO(order);
        });
      });
  }

  async findOne(id: string): Promise<FullOrderDto> {
    return this.ordersStorageService.findOne(id)
      .then((order) => {
        return this.ordersStorageService.documentToDTO(order);
      });
  }
}
