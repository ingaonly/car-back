import { CarInterface } from '@interfaces/car.interface';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { OrderInterface } from '@interfaces/order.interface';

export type OrderDocument = HydratedDocument<Order>;

@Schema()
export class Order implements OrderInterface {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Car' })
  car: CarInterface;

  @Prop()
  name: string;

  @Prop()
  phone: string;

  @Prop({ type: Date, required: true, default: Date.now })
  date: Date;
}

export const OrderSchema = SchemaFactory.createForClass(Order);
