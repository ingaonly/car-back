import { FullCarDto } from '@api/cars/dto/full-car.dto';

export class FullOrderDto {
  car: FullCarDto;

  readonly name: string;

  readonly phone: string;

  readonly date?: Date;
}
