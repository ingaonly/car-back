import { FullOrderDto } from '@api/orders/dto/full-order.dto';

export class CreateOrderDto extends FullOrderDto  {
  car_id: string;
}
