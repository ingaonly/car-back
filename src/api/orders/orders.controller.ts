import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  UseGuards,
  HttpException,
  HttpStatus, Logger,
} from '@nestjs/common';
import { OrdersService } from './orders.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { JwtAuthGuardService } from '@services/authentitication/jwt-auth.guard/jwt-auth.guard.service';
import { isNil } from '@nestjs/common/utils/shared.utils';
import { CarsService } from '@api/cars/cars.service';
import { ApiBearerAuth, ApiOperation } from '@nestjs/swagger';

@Controller({
  path: 'orders',
  version: '1',
})
export class OrdersController {
  private readonly logger = new Logger(OrdersController.name);

  constructor(private readonly ordersService: OrdersService,
              private readonly carsService: CarsService,
  ) {
  }

  @ApiOperation({ summary: "Создание заказа" })
  @Post()
  create(@Body() createOrderDto: CreateOrderDto) {
    this.logger.debug(createOrderDto);
    if (isNil(createOrderDto.car_id)) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          errorText: 'Требуется указазать машину',
        },
        HttpStatus.BAD_REQUEST,
      );
    }


    return this.carsService.findOne(createOrderDto.car_id)
      .then((car) => {
        if (!car) {
          throw new HttpException(
            {
              status: HttpStatus.BAD_REQUEST,
              errorText: 'Автомобиль не найден по id',
            },
            HttpStatus.BAD_REQUEST,
          );
        }
        createOrderDto.car = car;
        this.logger.debug(car);

        return this.ordersService.create(createOrderDto)
          .catch((err) => {
            if (err.name === 'MongoServerError' && err.code === 11000) {
              throw new HttpException(
                {
                  status: HttpStatus.UNPROCESSABLE_ENTITY,
                  errorText: 'order already exists',
                },
                HttpStatus.UNPROCESSABLE_ENTITY,
              );
            }
          });
      })
      .catch((err) => {
        this.logger.error(err);
        throw new HttpException(
          {
            status: HttpStatus.BAD_REQUEST,
            errorText: 'Автомобиль не найден по id',
          },
          HttpStatus.BAD_REQUEST,
        );
      });
  }

  @ApiOperation({ summary: "Получение всех заказов" })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardService)
  @Get()
  findAll() {
    return this.ordersService.findAll();
  }

  @ApiOperation({ summary: "Получение заказа по id" })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardService)
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.ordersService.findOne(id);
  }

  // @UseGuards(JwtAuthGuardService)
  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto) {
  //   return this.ordersService.update(id, updateOrderDto);
  // }
  //
  // @UseGuards(JwtAuthGuardService)
  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.ordersService.remove(id);
  // }
}
