import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { OrdersStorageService } from '@services/storage/orders/orders-storage.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UsersModule } from '@users/users.module';
import { CarModelsModule } from '@api/car-models/car-models.module';
import { CarBrandsModule } from '@api/car-brands/car-brands.module';
import { Order, OrderSchema } from '@api/orders/schemas/order.schema';
import { CarsModule } from '@api/cars/cars.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Order.name, schema: OrderSchema }]),
    PassportModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        secret: config.get('JWT_SECRET'),
      }),
      inject: [ConfigService],
    }),
    UsersModule,
    CarModelsModule,
    CarBrandsModule,
    CarsModule,
  ],
  controllers: [OrdersController],
  providers: [OrdersService, OrdersStorageService],
})
export class OrdersModule {
}
