import { Body, Controller, Get, HttpException, HttpStatus, Logger, Param, Post, UseGuards } from '@nestjs/common';
import { CarModelsService } from './car-models.service';
import { CreateCarModelDto, CreateCarModelInternalDto } from './dto/create-car-model.dto';
import { isNil } from '@nestjs/common/utils/shared.utils';
import { CarBrandsService } from '@api/car-brands/car-brands.service';
import { JwtAuthGuardService } from '@services/authentitication/jwt-auth.guard/jwt-auth.guard.service';
import { ApiBearerAuth, ApiOperation } from '@nestjs/swagger';
import { CarModelDto } from '@api/car-models/dto/car-model.dto';


@Controller({
  path: 'car-models',
  version: '1',
})
export class CarModelsController {
  private readonly logger = new Logger(CarModelsController.name);

  constructor(private readonly carModelsService: CarModelsService,
              private readonly carBrandsService: CarBrandsService) {
  }

  @ApiOperation({ summary: "Создание модели" })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuardService)
  @Post()
  async create(@Body() createCarModelDto: CreateCarModelDto): Promise<void|CarModelDto> {
    this.logger.debug(createCarModelDto);
    const carModel = {
      name: createCarModelDto.name,
      brand_id: createCarModelDto.brand_id,
    } as CreateCarModelInternalDto;
    if (isNil(carModel.brand_id)) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          errorText: 'Требуется указать бренд',
        },
        HttpStatus.BAD_REQUEST,
      );
    }
    return this.carBrandsService
      .findOne(carModel.brand_id)
      .then((brand) => {
        this.logger.debug('fff', brand);
        carModel.brand = brand;
        return this.carModelsService.create(carModel)
          .catch((err) => {
            if (err.name === 'MongoServerError' && err.code === 11000) {
              this.logger.log('Car Model already exists: ' + carModel.name);
              throw new HttpException(
                {
                  status: HttpStatus.UNPROCESSABLE_ENTITY,
                  errorText: 'Такая модель уже существует',
                },
                HttpStatus.UNPROCESSABLE_ENTITY,
              );
            }
          });
      })
      .catch((err) => {
        this.logger.error(err);
        throw new HttpException(
          {
            status: HttpStatus.BAD_REQUEST,
            errorText: 'Бренд не найден по id',
          },
          HttpStatus.BAD_REQUEST,
        );
      });
  }

  @ApiOperation({ summary: "Получение всех моделей" })
  @Get()
  async findAll() {
    return this.carModelsService.findAll();
  }

  @ApiOperation({ summary: "Получение модели по id" })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.carModelsService
      .findOne(id)
      .then((model) => {
        if (isNil(model)) {
          throw new HttpException(
            {
              status: HttpStatus.NOT_FOUND,
              errorText: 'Модель не найдена',
            },
            HttpStatus.NOT_FOUND,
          );
        }
        return model;
      });
  }
}
