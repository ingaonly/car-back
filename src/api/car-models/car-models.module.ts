import { Module } from '@nestjs/common';
import { CarModelsService } from './car-models.service';
import { CarModelsController } from './car-models.controller';
import { CarModelsStorageService } from '@services/storage/car-models/car-models-storage.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UsersModule } from '@users/users.module';
import { CarModel, CarModelSchema } from '@api/car-models/schemas/car-model.schema';
import { CarBrandsModule } from '@api/car-brands/car-brands.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: CarModel.name, schema: CarModelSchema }]),
    PassportModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        secret: config.get('JWT_SECRET'),
      }),
      inject: [ConfigService],
    }),
    UsersModule,
    CarBrandsModule,
  ],
  controllers: [CarModelsController],
  providers: [
    CarModelsService,
    CarModelsStorageService,
  ],
  exports: [CarModelsService],
})
export class CarModelsModule {
}
