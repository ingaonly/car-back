import { Injectable } from '@nestjs/common';
import { CreateCarModelDto } from './dto/create-car-model.dto';
import { CarModelsStorageService } from '@services/storage/car-models/car-models-storage.service';
import { CarModelDto } from '@api/car-models/dto/car-model.dto';

@Injectable()
export class CarModelsService {

  constructor(private carModelsStorageService: CarModelsStorageService) {
  }

  async create(createCarModelDto: CreateCarModelDto): Promise<CarModelDto> {
    return this.carModelsStorageService.create(createCarModelDto)
      .then((carModel) => {
        return this.findOne(String(carModel._id));
      });
  }

  async findAll(): Promise<CarModelDto[]> {
    return this.carModelsStorageService.findAll()
      .then((carModels) => {
        return carModels.map((carModel) => {
          return this.carModelsStorageService.documentToDTO(carModel);
        });
      });
  }

  async findOne(id: string): Promise<CarModelDto> {
    return this.carModelsStorageService.findOne(id)
      .then((carModel) => {
        if (!carModel) {
          return null;
        }
        return this.carModelsStorageService.documentToDTO(carModel);
      });
  }
}
