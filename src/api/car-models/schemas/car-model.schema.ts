import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Schema as MSchema } from 'mongoose';
import { CarModelInterface } from '@interfaces/car-model.interface';
import { CarBrandInterface } from '@interfaces/car-brand.interface';

export type CarModelDocument = HydratedDocument<CarModel>

@Schema()
export class CarModel implements CarModelInterface {
  @Prop({ required: true, type: String, index: true })
  name: string;

  @Prop({ type: MSchema.Types.ObjectId, ref: 'CarBrand', index: true })
  brand: CarBrandInterface;
}


export const CarModelSchema = SchemaFactory
  .createForClass(CarModel)
  .index({ 'name': 1, 'brand': 1 }, { unique: true });