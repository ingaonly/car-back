import { CarModelInterface } from '@interfaces/car-model.interface';
import { CarBrandInterface } from '@interfaces/car-brand.interface';
import { ApiProperty } from '@nestjs/swagger';
import { CarBrandDto } from '@api/car-brands/dto/car-brand.dto';

export class CarModelDto implements CarModelInterface {
  @ApiProperty({ description: 'Бренд автомобиля', type: CarBrandDto })
  brand: CarBrandInterface;

  @ApiProperty({ example: 'X7', description: 'Модель автомобиля', type: 'string' })
  name: string;

  @ApiProperty({ example: '63cd7c5edea0d8da68249a68', description: 'ID модели', type: 'string' })
  _id: string;
}
