import { CarModelInterface } from '@interfaces/car-model.interface';
import { CarBrandInterface } from '@interfaces/car-brand.interface';
import { ApiProperty } from '@nestjs/swagger';
import { CarBrandDto } from '@api/car-brands/dto/car-brand.dto';

export class CreateCarModelDto  {
  @ApiProperty({ example: "63cd7c5edea0d8da68249a68", type: 'string', required: true, description: 'ID бренда'})
  brand_id?: string;

  @ApiProperty({ example: 'X7', description: 'Название модели', type: 'string', required: true })
  name: string;
}

export class CreateCarModelInternalDto  implements CarModelInterface {
  @ApiProperty({ example: CarBrandDto, description: 'The car model id' })
  brand: CarBrandInterface;

  @ApiProperty({ example: "63cd7c5edea0d8da68249a68", type: 'string', required: false, description: 'ID бренда'})
  brand_id?: string;

  @ApiProperty({ example: 'X7', description: 'Название модели', type: 'string', required: true })
  name: string;
}