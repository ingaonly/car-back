import { FullCarDto } from './full-car.dto';
import { ApiProperty } from '@nestjs/swagger';

export class CreateCarDto {
  @ApiProperty({ example: '63cd7c5edea0d8da68249a68', description: 'ID модели', required: true })
  readonly model_id: string;

  @ApiProperty({ example: '2023', description: 'Год выпуска автомобиля', required: true })
  readonly year: number;

  @ApiProperty({ example: 'синий', description: 'Цвет автомобиля', required: true })
  readonly color: string;

  @ApiProperty({ example: '999000', description: 'Цена автомобиля', required: true })
  readonly price: string;

  @ApiProperty({ example: 'длинное описание автомобиля', description: 'Описание автомобиля', required: true })
  readonly info: string;

  @ApiProperty({ description: 'Картинка автомобиля', required: false })
  file: string;
}

export class CreateCarInternalDto extends FullCarDto {
  @ApiProperty({ example: '63cd7c5edea0d8da68249a68', description: 'ID модели', required: true })
  readonly model_id: string;
}