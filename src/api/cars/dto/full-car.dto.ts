import { CarModelInterface } from '@interfaces/car-model.interface';
import { ApiProperty } from '@nestjs/swagger';
import { CarModelDto } from '@api/car-models/dto/car-model.dto';
import { UserDto } from '@users/dto/full-user.dto';

export class FullCarDto {
  @ApiProperty({ description: 'Модель автомобиля', type: CarModelDto })
  model: CarModelInterface;

  @ApiProperty({ example: '2023', description: 'Год выпуска автомобиля', required: true })
  readonly year: number;

  @ApiProperty({ example: 'синий', description: 'Цвет автомобиля', required: true })
  readonly color: string;

  @ApiProperty({ example: '999000', description: 'Цена автомобиля', required: true })
  readonly price: string;

  @ApiProperty({ example: 'длинное описание автомобиля', description: 'Описание автомобиля', required: true })
  readonly info: string;

  @ApiProperty({ example: 'foobar.png', description: 'Адрес картинки автомобиля', type: 'string', required: false })
  img: string;

  @ApiProperty({ description: 'Менеджер автомобиля', type: UserDto })
  manager?: UserDto;

  @ApiProperty({ example: '63cd7c5edea0d8da68249a68', description: 'ID автомобиля', type: 'string' })
  _id: string;
}
