import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  UseGuards,
  Request,
  HttpException,
  HttpStatus, UseInterceptors,
} from '@nestjs/common';
import { CarsService } from './cars.service';
import { CreateCarDto, CreateCarInternalDto } from './dto/create-car.dto';
import { JwtAuthGuardService } from '@services/authentitication/jwt-auth.guard/jwt-auth.guard.service';
import { UsersService } from '@users/users.service';
import { CarModelsService } from '@api/car-models/car-models.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { isNil } from '@nestjs/common/utils/shared.utils';
import { CarBrandsService } from '@api/car-brands/car-brands.service';
import { ApiBearerAuth, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { FullCarDto } from '@api/cars/dto/full-car.dto';

@Controller({
  path: 'cars',
  version: '1',
})
export class CarsController {
  constructor(private readonly carsService: CarsService,
              private readonly userService: UsersService,
              private readonly carBrandsService: CarBrandsService,
              private readonly carModelsService: CarModelsService) {
  }

  static imgName: string;

  @ApiOperation({ summary: 'Создание машины' })
  @ApiBearerAuth()
  @ApiResponse({
    status: 200,
    type: FullCarDto,
  })
  @UseGuards(JwtAuthGuardService)
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/',
        filename: (req, file, cb) => {
          const imgType = file.mimetype.split('/');
          const uniqSuffix = Date.now() + Math.round(Math.random() * 1e9);
          const imgName = `${file.fieldname}-${uniqSuffix}.${imgType[1]}`;
          cb(null, imgName);
          CarsController.imgName = imgName;
        },
      }),
    }),
  )
  @Post()
  async create(@Body() createCarDto: CreateCarDto, @Request() req): Promise<FullCarDto> {
    const car = {
      model_id: createCarDto.model_id,
      price: createCarDto.price,
      year: createCarDto.year,
      info: createCarDto.info,
      color: createCarDto.color,
    } as CreateCarInternalDto;
    const user = await this.userService.findOneByID(req.user.id);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          errorText: 'Пользователь не найден',
        },
        HttpStatus.CONFLICT,
      );
    }
    car.manager = user;

    const model = await this.carModelsService.findOne(car.model_id);
    console.log(model);
    if (!model) {
      throw new HttpException(
        {
          status: HttpStatus.CONFLICT,
          errorText: 'Модель не найдена',
        },
        HttpStatus.CONFLICT,
      );
    }
    car.model = model;
    car.img = CarsController.imgName;
    const res = this.carsService.create(car)
    CarsController.imgName = null;
    return res.catch((err: HttpException) => {
      throw new HttpException(
        {
          status: err.getStatus(),
          errorText: err.getResponse(),
        },
        err.getStatus(),
      );
    });
  }

  @ApiOperation({ summary: 'Получение всех машин' })
  @Get()
  @ApiResponse({
    status: 200,
    type: FullCarDto,
    isArray: true,
  })
  findAll() {
    return this.carsService.findAll();
  }

  @ApiOperation({ summary: 'Получение машины по id' })
  @Get(':id')
  @ApiResponse({
    status: 200,
    type: FullCarDto,
  })
  async findOne(@Param('id') id: string) {
    return this.carsService.findOne(id)
      .then((car: FullCarDto) => {
        console.log(car);
        if (isNil(car)) {
          throw new HttpException(
            {
              status: HttpStatus.NOT_FOUND,
              errorText: 'Машина не найдена',
            },
            HttpStatus.NOT_FOUND,
          );
        }
        return car;
      });
  }
}
