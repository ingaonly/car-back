import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateCarInternalDto } from './dto/create-car.dto';
import { CarsStorageService } from '@services/storage/cars/cars-storage.service';
import { FullCarDto } from '@api/cars/dto/full-car.dto';

@Injectable()
export class CarsService {

  constructor(private readonly carsStorageService: CarsStorageService) {
  }

  async create(createCar: CreateCarInternalDto): Promise<FullCarDto> {
    if (createCar.year >= new Date().getFullYear()) {
      throw new HttpException(
        'Год выпуска не может быть больше текущего',
        HttpStatus.BAD_REQUEST,
      );
    }
    return this.carsStorageService.create(createCar);
  }

  async findAll(): Promise<FullCarDto[]> {
    return this.carsStorageService.findAll();
  }

  async findOne(id: string): Promise<FullCarDto> {
    return this.carsStorageService.findOne(id);
  }
}