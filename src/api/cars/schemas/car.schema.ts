import { CarInterface } from '@interfaces/car.interface';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { User } from '@users/schemas/user.schema';
import { CarModelInterface } from '@interfaces/car-model.interface';

export type CarDocument = HydratedDocument<Car>;

@Schema()
export class Car implements CarInterface {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'CarModel' })
  model: CarModelInterface;

  @Prop()
  color: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  manager: User;

  @Prop()
  price: string;

  @Prop()
  year: number;

  @Prop()
  info: string;

  @Prop()
  img: string;
}

export const CarSchema = SchemaFactory.createForClass(Car);
