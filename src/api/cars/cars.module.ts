import { Module } from '@nestjs/common';
import { CarsService } from './cars.service';
import { CarsController } from './cars.controller';
import { CarsStorageService } from '@services/storage/cars/cars-storage.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthService } from '@services/authentitication/auth/auth.service';
import { JwtStrategyService } from '@services/authentitication/jwt-strategy/jwt-strategy.service';
import { Car, CarSchema } from './schemas/car.schema';
import { UsersModule } from '@users/users.module';
import { CarModelsModule } from '@api/car-models/car-models.module';
import { CarBrandsModule } from '@api/car-brands/car-brands.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Car.name, schema: CarSchema }]),
    PassportModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (config: ConfigService) => ({
        secret: config.get('JWT_SECRET'),
      }),
      inject: [ConfigService],
    }),
    UsersModule,
    CarModelsModule,
    CarBrandsModule,
  ],
  controllers: [
    CarsController,
  ],
  providers: [
    CarsService,
    CarsStorageService,
    AuthService,
    JwtStrategyService,
  ],
  exports: [CarsService],
})
export class CarsModule {
}
