import { Test, TestingModule } from '@nestjs/testing';
import { UsersStorageService } from './users-storage.service';

describe('UsersStorageService', () => {
  let service: UsersStorageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersStorageService],
    }).compile();

    service = module.get<UsersStorageService>(UsersStorageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
