import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from '@users/schemas/user.schema';
import { Model } from 'mongoose';
import { CreateUserDto } from '@users/dto/create-user.dto';
import { UpdateUserDto } from '@users/dto/update-user.dto';
import { UserDto } from '@users/dto/full-user.dto';

@Injectable()
export class UsersStorageService {
  private readonly logger = new Logger(UsersStorageService.name);

  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {
  }

  async create(createUserDto: CreateUserDto): Promise<UserDocument> {
    this.logger.debug('create user with email: ' + createUserDto.email);
    const userData = new this.userModel(createUserDto);
    return userData.save();
  }

  async findAll(): Promise<UserDocument[]> {
    return this.userModel.find();
  }

  async findOneByID(id: string): Promise<UserDocument> {
    return this.userModel.findById(id);
  }

  async findOneByEmailAndPassword(email: string, password: string): Promise<UserDocument> {
    this.logger.debug('find user with email: ' + email + ' and password');
    return this.userModel.findOne({ email: email, password: password });
  }

  async findOneByEmail(email: string): Promise<UserDocument> {
    return this.userModel.findOne({ email: email });
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<UserDocument> {
    return this.userModel.findByIdAndUpdate(
      id,
      updateUserDto,
      {
        new: true,
      });
  }

  async remove(id: string): Promise<UserDocument> {
    return this.userModel.findByIdAndRemove(id);
  }

  documentToDTO(user: UserDocument): UserDto {
    if (!user) {
      return null;
    }
    return {
      _id: String(user._id),
      name: user.name,
      phone: user.phone,
      email: user.email,
    } as UserDto;
  }
}
