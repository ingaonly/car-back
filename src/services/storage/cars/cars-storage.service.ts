import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Car, CarDocument } from '@api/cars/schemas/car.schema';
import { CreateCarInternalDto } from '@api/cars/dto/create-car.dto';
import { UpdateCarDto } from '@api/cars/dto/update-car.dto';
import { FullCarDto } from '@api/cars/dto/full-car.dto';
import { CarsStorage } from '@services/storage/cars/cars-storage.interface';

@Injectable()
export class CarsStorageService implements CarsStorage {

  constructor(@InjectModel(Car.name) private carModel: Model<CarDocument>) {
  }

  async create(createCar: CreateCarInternalDto): Promise<FullCarDto> {
    const carData = new this.carModel(createCar);
    return carData.save()
      .then((car) => {
        return this.documentToDTO(car);
      });
  }

  async findAll(): Promise<FullCarDto[]> {
    return this.carModel.find()
      .populate({
        path: 'model',
        model: 'CarModel',
        populate: {
          path: 'brand',
          model: 'CarBrand',
        },
      })
      .populate({
        path: 'manager',
        model: 'User',
      })
      .sort({ _id: -1 })
      .then((cars) => {
        return cars.map((car) => {
          return this.documentToDTO(car);
        });
      });
  }

  async findAllByManagerID(managerId: string): Promise<FullCarDto[]> {
    return this.carModel.find({ manager: managerId })
      .then((cars) => {
        return cars.map((car) => {
          return this.documentToDTO(car);
        });
      });
  }

  async findOne(id: string): Promise<FullCarDto> {
    return this.carModel.findById(id)
      .populate({
        path: 'model',
        model: 'CarModel',
        populate: {
          path: 'brand',
          model: 'CarBrand',
        },
      })
      .populate({
        path: 'manager',
        model: 'User',
      })
      .then((car) => {
        return this.documentToDTO(car);
      });
  }

  async update(id: string, updateCarDto: UpdateCarDto): Promise<FullCarDto> {
    return this.carModel.findByIdAndUpdate(
      id,
      updateCarDto,
      {
        new: true,
      })
      .then((car) => {
        return this.documentToDTO(car);
      });
  }

  private documentToDTO(car: CarDocument): FullCarDto {
    if (!car) {
      return null;
    }
    return {
      _id: String(car._id),
      model: car.model,
      manager: car.manager,
      color: car.color,
      year: car.year,
      price: car.price,
      info: car.info,
      img: car.img,
    } as FullCarDto;
  }
}