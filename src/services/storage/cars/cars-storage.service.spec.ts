import { Test, TestingModule } from '@nestjs/testing';
import { CarsStorageService } from './cars-storage.service';

describe('CarsStorageService', () => {
  let service: CarsStorageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CarsStorageService],
    }).compile();

    service = module.get<CarsStorageService>(CarsStorageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
