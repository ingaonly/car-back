import { CreateCarInternalDto } from '@api/cars/dto/create-car.dto';
import { FullCarDto } from '@api/cars/dto/full-car.dto';

export interface CarsStorage {
  create(createCar: CreateCarInternalDto): Promise<FullCarDto>;
  findAll(): Promise<FullCarDto[]>
  findAllByManagerID(managerId: string): Promise<FullCarDto[]>
  findOne(id: string): Promise<FullCarDto>
}