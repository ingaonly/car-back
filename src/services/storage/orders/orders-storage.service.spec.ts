import { Test, TestingModule } from '@nestjs/testing';
import { OrdersStorageService } from './orders-storage.service';

describe('OrdersStorageService', () => {
  let service: OrdersStorageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OrdersStorageService],
    }).compile();

    service = module.get<OrdersStorageService>(OrdersStorageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
