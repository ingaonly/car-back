import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Order, OrderDocument } from '@api/orders/schemas/order.schema';
import { CreateOrderDto } from '@api/orders/dto/create-order.dto';
import { FullOrderDto } from '@api/orders/dto/full-order.dto';

@Injectable()
export class OrdersStorageService {
  constructor(@InjectModel(Order.name) private orderModel: Model<OrderDocument>) {
  }

  async create(createOrderDto: CreateOrderDto): Promise<OrderDocument> {
    const orderData = new this.orderModel(createOrderDto);
    return orderData.save();
  }

  async findAll(): Promise<OrderDocument[]> {
    return this.orderModel.find()
      .populate({
        path: 'car',
        model: 'Car',
        populate: [
          {
            path: 'model',
            model: 'CarModel',
            populate: {
              path: 'brand',
              model: 'CarBrand',
            },
          },
          {
            path: 'manager',
            model: 'User',
          },
        ],
      });

  }

  async findOne(id: string): Promise<OrderDocument> {
    return this.orderModel.findById(id);
  }

  documentToDTO(order: OrderDocument): FullOrderDto {
    if (!order) {
      return null;
    }
    return {
      _id: String(order._id),
      car: order.car,
      name: order.name,
      phone: order.phone,
      date: order.date,
    } as unknown as FullOrderDto;
  }
}
