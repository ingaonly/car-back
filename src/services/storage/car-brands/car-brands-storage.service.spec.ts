import { Test, TestingModule } from '@nestjs/testing';
import { CarBrandsStorageService } from './car-brands-storage.service';

describe('CarBrandStorageService', () => {
  let service: CarBrandsStorageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CarBrandsStorageService],
    }).compile();

    service = module.get<CarBrandsStorageService>(CarBrandsStorageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
