import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CarBrand, CarBrandDocument } from '@api/car-brands/schemas/car-brand.schema';
import { CreateCarBrandDto } from '@api/car-brands/dto/create-car-brand.dto';
import { UpdateCarBrandDto } from '@api/car-brands/dto/update-car-brand.dto';
import { CarBrandDto } from '@api/car-brands/dto/car-brand.dto';

@Injectable()
export class CarBrandsStorageService {
  constructor(@InjectModel(CarBrand.name) private carBrandModel: Model<CarBrandDocument>) {
  }

  async create(createCarBrand: CreateCarBrandDto): Promise<CarBrandDocument> {
    const carBrandData = new this.carBrandModel(createCarBrand);
    return carBrandData.save();
  }

  async findAll(): Promise<CarBrandDocument[]> {
    return this.carBrandModel.find();
  }

  async findByIDs(ids: string[]): Promise<CarBrandDocument[]> {
    return this.carBrandModel.find({ _id: { $in: ids } });
  }

  async findOne(id: string): Promise<CarBrandDocument> {
    return this.carBrandModel.findById(id);
  }

  async update(id: string, body: UpdateCarBrandDto): Promise<CarBrandDocument> {
    return this.carBrandModel.findByIdAndUpdate(id, body, {new: true});
  }

  documentToDTO(carBrand: CarBrandDocument): CarBrandDto {
    if (!carBrand) {
      return null;
    }
    return {
      _id: String(carBrand._id),
      name: carBrand.name,
    } as CarBrandDto;
  }
}
