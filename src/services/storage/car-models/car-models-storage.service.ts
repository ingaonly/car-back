import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CarModel, CarModelDocument } from '@api/car-models/schemas/car-model.schema';
import { CreateCarModelDto } from '@api/car-models/dto/create-car-model.dto';
import { CarModelDto } from '@api/car-models/dto/car-model.dto';

@Injectable()
export class CarModelsStorageService {
  constructor(@InjectModel(CarModel.name) private carModelModel: Model<CarModelDocument>) {
  }

  async create(createCarModelDto: CreateCarModelDto): Promise<CarModelDocument> {
    const carBrandData = new this.carModelModel(createCarModelDto);
    return carBrandData.save();
  }

  async findAll(): Promise<CarModelDocument[]> {
    return this.carModelModel.find()
      .populate({
        path: 'brand',
        model: 'CarBrand',
      })
  }

  async findOne(id: string): Promise<CarModelDocument> {
    return this.carModelModel.findById(id)
      .populate({
        path: 'brand',
        model: 'CarBrand',
      })
  }

  documentToDTO(carModel: CarModelDocument): CarModelDto {
    if (!carModel) {
      return null;
    }
    return {
      _id: String(carModel._id),
      name: carModel.name,
      brand: carModel.brand,
    } as CarModelDto;
  }
}
