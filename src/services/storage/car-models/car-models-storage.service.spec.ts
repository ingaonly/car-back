import { Test, TestingModule } from '@nestjs/testing';
import { CarModelsStorageService } from './car-models-storage.service';

describe('CarModelsStorageService', () => {
  let service: CarModelsStorageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CarModelsStorageService],
    }).compile();

    service = module.get<CarModelsStorageService>(CarModelsStorageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
