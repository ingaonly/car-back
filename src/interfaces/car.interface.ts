import { UserInterface } from '@interfaces/user.interface';
import { CarModelInterface } from '@interfaces/car-model.interface';

export interface CarInterface {
  model?: CarModelInterface;
  year: number;
  color: string;
  price: string;
  info: string;
  img: string;
  manager?: UserInterface;
}
