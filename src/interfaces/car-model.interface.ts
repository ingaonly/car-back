import { CarBrandInterface } from '@interfaces/car-brand.interface';

export interface CarModelInterface {
  name: string;
  brand: CarBrandInterface;
}
