import { CarInterface } from '@interfaces/car.interface';

export interface OrderInterface {
  car: CarInterface;
  name: string;
  phone: string;
  date?: Date;
}
